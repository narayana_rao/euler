# A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

# Find the largest palindrome made from the product of two 3-digit numbers.

def reverseNumber(nNumber):
    nReverse = 0;
    while nNumber>0:
        nReminder = nNumber % 10
        nNumber = nNumber // 10
        nReverse = (nReverse*10)+nReminder        
    
    return nReverse

nMinRange = 100
nMaxRange = 1001
dicFebs = {}

for nStartVal in range(nMinRange, nMaxRange):
    for nNextVal in range(nStartVal, nMaxRange):
        nResult = nNextVal * nStartVal
        
        if(nResult == reverseNumber(nResult)) :
            dicFebs[nResult] = str(nStartVal) +'x'+str(nNextVal)

print(max(dicFebs))


# 102 => 102 / 10 => 10 - 2 = 0*10+2
# 10 => 10/ 10 => 1 - 0 = 2*10+0
# 1 => 1/10 => 0 - 1 = 20*10+1