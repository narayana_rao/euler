# 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

def gcd(x,y): return y and gcd(y, x % y) or x
def lcm(x,y): return x * y / gcd(x,y)

n = 1
for i in range(1, 21):
     n = lcm(n, i)
print(n)

# flag = True
# k = 1
# while flag:
#     for i in range(1 , 21):
#         residuo = k % i
#         if residuo == 0:
#             divisor = True
#         else:
#             divisor = False
#             break
#     if divisor:
#         print(k)
#         flag = False
#     else:
#        k += 1


#232792560 https://www.w3resource.com/python-exercises/challenges/1/python-challenges-1-exercise-37.php

#------------------------------------------------------------------------------------------------------------------------


#factors of a number

# nValue = 10
# decFactors = []

# for nStart in range(1,21):
#     for nVal in range(2,nStart+1):
#         if not nStart % nVal :
#             decFactors.append( nVal)
#             break

# print(decFactors)