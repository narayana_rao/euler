# If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

# Find the sum of all the multiples of 3 or 5 below 1000.
nRange = 1001
nSum = 0

for nValue in range(1,nRange):
    if not nValue % 3:
        nSum += nValue
    elif not nValue % 5 :
        nSum += nValue
    
print("total sum is" + str(nSum))
#234168
#233168
