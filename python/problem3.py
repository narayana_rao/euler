# The prime factors of 13195 are 5, 7, 13 and 29.

# What is the largest prime factor of the number 600851475143 ?

import time;

tm = time.time()
nTarget = 600851475143
lsFactors = [];

def isPrime(nNum):
    for nValue in range(2, nNum):
        if not nNum % nValue :
            return False
        
    return True

for nValue in range(1, (nTarget//2)):
    if nTarget % nValue == 0 :
        if isPrime(nValue):
            lsFactors.append(nValue)
            print(nValue)

if len(lsFactors) > 0 :
    print(max(lsFactors))
else :
    print("no..")

tm2 = time.time()
print("time is", tm2-tm)

#6857